# Projeto BarrilDobrado
O projeto BarrilDobrado é um robô iniciante, mas com grande pontencial de supreender em batalhas!

## Pontos Fortes:
1. É um robô que sabe a hora certa de atirar:
- Isso foi extremamente importante no processo de criação, revirando a API e vendo inúmeros tutoriais na internet descobri que a cada disparo o robô perde a mesma quantidade de vida. Porém ele recupera essa vida caso atinja algum outro robô, então a solução foi determinar o momento exato do disparo.
2. Não ficar parado:
- Utilizar o AdvancedRobot fez com que eu pudesse programar para que o BarrilDobrado não parasse de se movimentar, evitando assim ser atingido por inimigos que deflagram grandes quantidades de disparos parado.
3. Tiro de sorte:
- Ao colidir com os inimigos, o robô tem que parar, sendo assim um ótimo momento para tentar acertar qualquer outro inimigo, recuperando parte da vida perdida.

## Pontos Fracos:
1. Walls:
- Sim, o famoso "Walls", um rôbo programado para ficar rodando as paredes e atirar apenas para frente. Eu tinha duas opções, a primeira era programar um "Walls 2.0" ou ficar em uma das diagonais das paredes atirando nele, mas a há problemas nas duas opções, o problema da primeira é tornar a competição extremamente exaustiva e monotona, e da segunda é que em uma arena com outros robos eu seria um alvo fácil.
2. Paredes preso com inimigos:
- Esse foi um dos problemas que não consegui encontrar uma solução, caso o BarrilDobrado fique preso entre uma parede e um inimigo a derrota é certa.

# O que o projeto me ensinou:
Estou no 1º semestre do curso de ADS (Analise e desenvolvimento de sistemas) e ainda não tinha nem cogitado começar algo em Java ou C#, no momento tive aulas apenas de MySql e Python (que estou amando), então foi muito complicado entender e começar esse projeto e então resovi listar e explicar minhas dificuldades.
1. API muito "bagunçada":
- Coloquei entre aspas porque não posso afirmar se realmente está bagunçada ou é porque não estou totalmente familiarizado ainda com essa carreira que escolhi, estudando Python e MySql encontro mais artigos, blogs, foruns e vídeos explicativos, mesmo que sejam em inglês e senti falta destes itens no processo de aprendizagem, todavia, esses empecilhos me mostraram que nem tudo na carreira de T.I vai ser um mar de rosas com tudo explicado nos seus mínimos detalhes e isso me deu um novo fôlego
2. Burnout:
- Durante meu primeiro semestre já tive alguns estresses desnecessários e que fui melhorando com o tempo, e a criação desse rôbo me ajudou bastante pois entendi que eu posso quebrar e gritar com o mundo que o problema não vai ser resolvido, meus maiores passos foram dados depois de me acalmar e tentar ver de um outro ângulo.
3. Opiniões:
- Nem sempre você precisa de uma opinião de quem já está na área e posso constatar essa teoria durante esse projeto. As pessoas que mais me ajudaram foram minha mãe e meus amigos, apenas observando as batalhas nas vezes que os chamei demonstrar como estava o andamento do projeto e foi em alguma dessas vezes que consertei a movimentação, a precisão do tiro e até um bug que o BarrilDobrado ficava colidindo com a parede. Mesmo sendo pessoas que não entendem o processo de criação elas conseguiram exergar melhorias e bugs onde eu criador não conseguia.