package projeto1;
import robocode.*;
import java.awt.Color;
public class BarrilDobrado extends AdvancedRobot
{
	public void run() {
		setColors(Color.black,Color.blue,Color.red); // body,gun,radar
		setGunColor(Color.blue);
		while(true) {
			setAhead(150);
			setTurnRight(55);
			setTurnGunRight(8);
			execute();
			//Evitar ficar parado
		}
	}
	public void onScannedRobot(ScannedRobotEvent e) {
		if ((e.getDistance() < 200) && (e.getEnergy() > 30)){
			fire(3);
		/*Caso a distância seja menor que 300 e o HP esteja maior que 30
		 * Atirar com potência MAXIMA
		 * Isso evita disparos desnecessários
		 */
		}
		else if(e.getEnergy() < 20){
			fire(1);
		//Quando a energia estiver muito baixa, diminuir a força do disparo
		}
	}
	public void onHitByBullet(HitByBulletEvent e) {
		turnLeft(90 - e.getBearing());
	}
	public void onHitWall(HitWallEvent e) {
		turnRight(90);
		ahead(100 - e.getBearing());
	}
	public void onHitRobot(HitRobotEvent e){
		fire(1);
		//Tiro de sorte
		turnLeft(90 - e.getBearing());
		ahead(100);
	}
}
